package String;

public class occurrenceOf_O {

	public static void main(String[] args) {
	    String string = "Hello World";
	    int gotfirst = string.indexOf("o");
	    int gotlast = string.lastIndexOf("o");
	    System.out.println("First occurrence of letter 'o': " + gotfirst);
	    System.out.println("Last occurrence of letter 'o': " + gotlast);
	  }
}
