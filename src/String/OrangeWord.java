package String;

import java.util.Scanner;

public class OrangeWord {
public static void main(String[] args) {
	
	 Scanner input = new Scanner(System.in);
	    System.out.print("Enter a string: ");
	    String str = input.nextLine();
	    
	    if (str.contains("orange")) {
	      System.out.println("The string contains the word 'orange'");
	    } else {
	      System.out.println("The string does not contain the word 'orange'");
	    }
}
}
